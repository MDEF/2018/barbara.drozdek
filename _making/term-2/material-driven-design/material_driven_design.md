---
title: Material Driven Design
period: 24 January - 21 February 2019
date: 2019-01-28
term: II
published: true
---

![]({{site.baseurl}}/good_waste.jpg)

# **orange [peel]**

"Orange: an almost perfect object in which there is absolute consistency between form, function and consumption is found." -Bruno Munari

An orange is constructed of a series of modulated pieces in the form of orange section placed in the circle around a central vertical on which each section rests on its straight side, with all of curves sides turned towards the outside giving the whole almost form of sphere.

Peel is gathering all of sections together in a packing that is well differences in material and colour. It is quite hard from the outside and covered with a soft lining of protection that lies between the outside and the ensemble of the peaces. It is to create an neutral layer between the outside surface and the inner pieces with juice. It allows us when breaking the surface at any points without any need of calculating it exacted thickness. But anyway to open the packing is very simple and it is no necessary to attach printed instruction to the product.

![]({{site.baseurl}}/bruno_orange.jpg)

As it is being used today, the packing is not made to return to the manufacturer, you can not also composed it because it will make your compost too acid what in the end can kill your worms in the container. In the end the only choose that you have is to throw it away. But maybe there is another option. See below. Besides two layers and the segments inside where you can usually find a small seed. It is like a little gift that from the same tree production offers to the consumer. It is an opportunity for the consumer to have/start his own personal production of orange. This is a perfect example of economical unselfishness of nature in this idea that in the same time build a relation between consumer and production. You can plant your orange seed or not it is your choice. But the offer of this concession and the opportunity to do it. It frees to consumer from any frustration complex and established a relationship reciprocal trust.

# **process**

# *desk research*

**-->industry/production**

The orange is a hybrid between pomelo 48% and mandarin 52%. Like most citrus plants, oranges do well under moderate temperatures between 16-29 °C and require considerable amounts of sunshine and water. More than 73 tons of orange are produced on earth every year. Brazil is the world's leader of orange producer 17 million tonnes, followed by China, India, and the United States (Spain is on the sixth position) in the ranking made in 2016 by FAOSTAT (Food and Agriculture Organization Corporate Statistical Database) of the United Nations. Almost 99% of the fruit is processed for export.

main products:

- orange juice
- frozen orange juice concentrate
- sweet orange oil
- marmalade

The thick bitter layer can be processed into animal feed by desiccation process, using pressure and heat. It also is used as a food flavoring. The outermost layer of the rind can be thinly grated to produce orange zest. The white part of the rind with the pith, is a source of pectin and has nearly the same amount of vitamin C as the inter segments of orange.

![]({{site.baseurl}}/orange_peel.png)
*[citruspedia](https://www.sanpellegrinofruitbeverages.com/us/citrus/citruspedia)*

**-->chemical composition**

Orange peel is edible and has significant contents of vitamin C as I mentioned before, dietary fiber, total polyphenols, carotenoids, limonene and dietary minerals, like potassium and magnesium.

![]({{site.baseurl}}/chemical_composition.png)
*[source](https://www.researchgate.net/publication/284233276_Bioprocessing_of_citrus_waste_peel_for_induced_pectinase_production_by_Aspergillus_niger_its_purification_and_characterization)*

**->research based on senses and physical experiments**

experiments
- burns easily when is dry
- when the peel is wet the oil inside the exterior layer is inflammable as well
- absorbes smell
- floats on the water
- when it is heated with warm air it gets more flexible than at room temperature

senses
- the oil in the skin tears in the eyes
- it is bitter
- it's flexible up to a point
- after drying, the oil leaves an unpleasant matt layer on the skin
- the inner (white) layer has fiber
- the outer layer is divided into visible cells
- it has very intense concentrated smell

**->1st touch**

![]({{site.baseurl}}/powder.jpg)
*- drying process/ making an orange peel waste powder*

![]({{site.baseurl}}/pulp.jpg)
*- blending process/ making an orange peel waste pulp*

![]({{site.baseurl}}/drying_grinding_sitering.jpg)

- drying 50°C / grinding / sintering 30-45°C

I started with idea of sintering the orange powder. I took the idea from the additive fabrication process that takes place in 3D printing technology using the SLA method. I was hoping to replace the powder that is used in this method for more sustainable.

-->While trying to sinter orange powder with soldering iron, I found out that already at 45°C the powder began to burn.

![]({{site.baseurl}}/blending_drying.jpg)

- fresh orange / blending / drying in an oven 50°C

I blended the fresh orange peel waste. The consistency of the pulp was smooth to the extent that the intense orange colour of the outer peel layer was barely visible. After drying the pulp in a 50°C oven, the fibers became more visible.

--> The material that formed was broken in the top layer of the sample. It is quite stiff, it is not easy to break it with your hand.

![]({{site.baseurl}}/blending_cookingincloth.jpg)

- fresh orange / blending / wrap in a cloth / cooking in water (3h) / drying in an oven 50°C

I decided to cook in a water blended pulp. To avoid mixing up the pulp with excessive amount of water, I decided to use a material in which I could safely pour the pulp into the boiling water. The cooked pulp was dried in the oven at 50°C.

-->The consistency of the pulp after cooking is smooth with a visible small amount of fiber. After drying at 50°C you can see that the pulp was probably cracked due to the water content being too high.

![]({{site.baseurl}}/cooking_blending_drying.jpg)

- fresh orange / cooking in water (3h) / blending / drying in an oven 50°C

I cooked in a water fresh orange peel waste. The second step was a blending process as a result, a processed mass was in the consistency of pure. The pulp was dried in the oven at 50°C.

-->The result was unexpected the sample shrunk by more or less 60%, it was cracked and took on an unexpected shape after drying.

![]({{site.baseurl}}/alcohol_bath.jpg)

- dry/fresh orange peel —> alcoholic bath

To separate the oil that is in the outer layer of the orange peel, I used an alcoholic bath. In this process I tried with both the fresh orange peel and the one that had been dried before. It is necessary to leave the it covered in this way for a few days (minimum three days), covered by alcohol. It is important to mix the contents during that time.

-->The experiment works, but does not separate the oil from the skin in 100%. I decided, however, that I want to work with the material and not against it. Even if the oil contained in it is not sticking or joining the orange peel waste with other materials.

-->*After first week exploration of orange peel. I started adding natural binders to improve the properties of orange peel waste. Not forgetting at the same time about proportion and stay to be aware of the material. I was trying to add minimum amount of binders to the orange powder and fresh orange pulp as well. In the same time we all know that woman are multitasked l was trying to scale up and start working on 3D shapes paying attention on the control the material during the process of drying.
Remember be aware of what are you looking for!*

**->2nd round**

![]({{site.baseurl}}/caseine.jpg)

1.orange peel powder(2) & casein(1) & water

- mixed casein with water and orange powder, put it in the oven 150°C to let it dry

-->the sample shrunk and cracked during the drying process, an interesting pattern was created, the sample is very fragile

2.orange peel pulp(2) & casein(1)

- mixed casein with orange pulp, put it in the oven 150°C to let it dry

-->the sample did not crack during drying, but it is so brittle that it burst when it was pulled out of the mold

---

![]({{site.baseurl}}/galatine_vinegar_pulp.jpg)

1.orange peel pulp(2) & gelatine(1) & vinegar(1)

- mix all the ingredients then dry in an oven 150°C

--> the top layer of the sample was parched, the air gathered on the bottom of the mold, the sample is very fragile

2.orange peel pulp(3) & gelatine(1) & vinegar(1)

- mix all the ingredients then dry in an oven 150°C

--> the top layer of the sample was parched, the air gathered on the bottom of the mold, the sample is fragile

---

![]({{site.baseurl}}/gelatine_vinegar_powder&rasin_powder.jpg)

1.orange peel powder(2) & gelatine(1) & vinegar(1)

- mix vinegar with gelatine and heat it up on the stove, add powder and mix very fast, put it into the mold and close it with pressing with the same shape mold

-->the sample is very stiff but it is probably due to excess gelatin that gathered at the bottom of the form

2.orange peel powder(2) & rasin(1)

- mix the orange powder and resin in the form of powder, put in the oven 150°C

-->the ingredients have merged but the sample is quite fragile

---

![]({{site.baseurl}}/resin_pulp.jpg)

1.orange peel pulp(2) & resin(1)

- mix the orange pulp and resin in the form of powder, put in the oven 150°C

-->the top layer of the sample was parched, the air gathered on the bottom of the mold, the sample is very fragile

2.orange peel pulp(3) & resin(1)

- mix the orange pulp and resin in the form of powder, put in the oven 150°C

-->the ingredients have merged but the sample is still quite fragile

---

![]({{site.baseurl}}/resin_oil_powder.jpg)

1.orange peel powder(2) & sunflower oil(1) & resin(1)

- mix all the ingredients then put it in the oven 150°C

-->the sample due to excess oil after being removed from the furnace was not a uniform form and began to disintegrate

— orange peel powder(3) & sunflower oil(1) & resin(1)

2.mix all the ingredients then put it in the oven 150°C

-->the sample due to excess oil after being removed from the furnace was not a uniform form and began to disintegrate, during the stay in the furnace, the material contacting the form began to burn

---

![]({{site.baseurl}}/wax_binder.jpg)

1.orange peel powder(1) & beeswax(1)

- melt the beeswax in the metal pot then add orange powder constantly mixing

-->the resulting sample forms a uniform object, on top you can see the layer of excess wax that has accumulated

2.orange peel powder(2) & beeswax(1)

- melt the beeswax in the metal pot then add orange powder constantly mixing

-->the resulting sample contains too little wax, it disintegrates and does not form an uniform

---

*After one week of adding and experimenting with different natural binders based on protein, lipids and gelatine. Nothing was noteworthy for me and I continued to devote my time to finding the right binder. Maybe with one exception of combining beeswax and the orange powder. I decided to take a step back and continue to work with pure orange peel waste. This time, focus on the fabrication, control process of drying, molding, injection and better understanding of the material during these processes.*

**->3rd attempt**

---

![]({{site.baseurl}}/molding_wax.jpg)

- orange peel powder & beeswax - molding on a ceramic flowerpot covered with a layer of baking paper

Melted beeswax in a ratio of 1: 1 mixed it with orange powder. Then, using a metal spoon I was covering pre-prepared mold made out of flower pot covered with the layer of baking paper. The paste of mixed beeswax and orange powder dried up quickly. After the process is finished you can easily take it out of the mold.

-->The mixture in ration 1:1 is good in working at room temperature. The object is waterproof so it does it's function as a cup.

---

![]({{site.baseurl}}/injection_wax.jpg)

- orange peel powder & beeswax - injection into the water

Melted beeswax in a ratio of 1: 1 mixed it with orange powder. Then, using a syringe, I tried to inject the mixture into a bottle with water. The first contact of the mixture with water causes a solid to be created by changing temperatures from the liquid.

-->The experiment was unsuccessful because the proportions of these two components were not properly selected. The mixture began to disintegrate in water.

---

![]({{site.baseurl}}/injection.jpg)

- orange peel pulp - extrusion

For the extrusion process I use a long-blended orange peel pulp which has the consistency of a paste. For extrusion, I used a 100ml syringe. I started with long strips of material and then tried to form more 3D forms with the basis of the circle. All forms were then dried in the oven at 50°C.

-->Each of samples has shrunk after drying. - None of the forms with a small cross-section did not cracked, which was quite surprising and gives hope for better results in the mechanized 3D printing process. The process that I would like to continue.

---

![]({{site.baseurl}}/sheet.jpg)

- orange peel pulp sheet

l blended the orange peels and rolled the pulp in between two layers of baking paper, then I put it into the oven to let it dry in temperature 50°C.

-->The sheet was deformed during the process of drying but the sheet that was made is a bit flexible so it is not so easy to break it.

---

![]({{site.baseurl}}/meshes.jpg)

orange peel pulp - molding on the round mold between two sheets of metal mesh and two sheets of baking paper

l blended the orange peels and rolled the pulp in between two layers of baking paper. Then I put it in between to sheets of metal mesh that was before shaped into a round form. I put it into the oven to let it dry in temperature 50°C.

-->The first big scale object that l made. on the bottom of the bowl the layer of material was thicker than the edges. Probably the thickness of the material is the reason why the object breaks or not. Also a big influence on how the material behaves is in the shape of the mold.

---

![]({{site.baseurl}}/cone.jpg)

- orang peel pulp - molding in an aluminum mold

The pure pulp l put into the aluminum mold. I put it with the mold into the oven to dry it.

-->The resulting sample was very skinned. It began to burn through the long contact of the pulp with a metal form that also gave back and sustained heat during the drying process.

---

![]({{site.baseurl}}/heating.jpg)

- orange peel - experimenting with a temperature

An experiment consisting in checking the properties of an orange peel in its original form. I check how the skin behaves after sunbathing or just after heating. After heating, the skin appears to be more flexible. The structure of the outer layer is also more visible. You can even carbonize it to such an extent that the layers will start to stratify.

-->The most interesting phenomenon, however, was the observation of how the spittle of a wrinkled peel is gouged with oil, which I so much wanted to get rid of in the first place. It turns out that it is easily flammable.

---

![]({{site.baseurl}}/briqette.jpg)

- orange peel pulp briquet

After I noticed that the material after drying is easily flammable and the oils that are contained in the skin are also easily flammable. I decided to use clean material for the production of briquettes. In this process, I used vacuum (vacuum cleaner) and one part of the Italian coffee machine, which I used as a form and part of which I could suck air from the pulp. I carried out the drying process in three forms to study and better understand the drying of solid form. The first is drying at a constant temperature of 50°C in the oven. In the second method, the sample was dried in an oven at 150°C. In the third method, I decided to first sample the outside with warm air using the heating gun, then the sample was put in an oven 150 °C.

--> The first sample has shrunk most of all, there are also traces of crack on it. The second sample of the excise is least compared to the other two, there are the most cracks on it. In the third sample there are no cracks but it is concave. * I am planning to continue the experiment in this direction, testing the calorific value of the briquette during the burning also developing the optimal process of briquette production.

---

**feedback/reflection/presentation**

![]({{site.baseurl}}/presentation.jpg)

*The material matters in every aspect of the designer's work. Before, during and after the design process. The designer is responsible for design decisions and should be aware what kind of consequences these decisions bring. The impact of the design for example water consumption, transportation etc. By using a mix of scientific and phenomenology methods we can define/feel the material.  Where it exist in the nature? What is its potential? Research by experimenting that is my favourite part of design process - testing. Checking how does it behave in different conditions engaging all of the senses not only mine but also others to see others perspective.  At the beginning I was analyzing what I already know and then l started to do as many experiments as l could. But still I was keeping the point That I want to stay with the pure material as long as l can. How should I start exploring it? But l did not think about final product. With the passage of time and accumulated experience, I started to create a plan of action for each day so that I could conduct several experiments at the same time. When you start reading/feeling your material you feel more confident in action. I noticed that in this process of working with new material based on biomass I skipped and I thing almost all of us taking the course, the creation of the human hand in the process of making. We started to design things dedicated to fabrication.*

# **How to understand craft behind technology?**

**possibilities on the future**

- **orange briquette**, checking energy value during the burning process - pure pulp
- **gelling substance** - pectine separation
- **package or single use tableware** - pure pulp
- **fiber** - beating/ keep the fiber length
