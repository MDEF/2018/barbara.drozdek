---
title: 07. computer-controlled machining
period: 07-13 March 2019
date: 2019-01-28
term: II
published: true
---

This week I want to fabricate my design project from the [week 2](https://mdef.gitlab.io/barbara.drozdek/fab_academy/week_2/). I designed parametric italic shelf using the Grasshopper. The main function of parametric programs is that you can easily change the parameters you define at the beginning of the script (usually), the size of the project as well. Together with [Adriana Tamargo Itturi](https://mdef.gitlab.io/adriana.tamargo/) we fabricated the shelf for her boyfriend to his new studio. He is a young architecture student and he need some furniture to store his works/books/models. He chose the parameters which were in line with his needs and the amount of space available in his studio.
With all of his chosen parameters we started the process of fabrication :)

![]({{site.baseurl}}/model.jpg)
*personalized model*

To prepare the file for fabrication I exported one face curves of each element form Grasshopper program to the Rhinoceros. Then I sorted out all the elements in Rhino. I turned each of the elements so that each of them had the same plane xy.

We decided to test the model using laser cutter. We fabricate the model in carboard 6mm thickness in a scale 1/5. (20% of the final project).

--> machine settings:

material: cardboard

thickness: 6mm

power: 50

speed: 1

Hz: 1000

![]({{site.baseurl}}/carboard_test.jpg)
*laser cutting test*

![]({{site.baseurl}}/carboard.jpg)
*cardboard test*

![]({{site.baseurl}}/cardboard_model.jpg)
*cardboard prototype*

In smaller scale it seemed to be ok, so we decided to fabricate the model in final scale. In the same rescaled file we added round corners to the inside cutting in ours elements. The diameter of the milling bit has 6mm so the pocketing diameter has to be a bit bigger for the machine to be able to make a circle in the corner we did it 6.1mm.

![]({{site.baseurl}}/radius.png)
*round corners/ inside cut*

The next stem is place the elements on the board. We used 2 boards because our elements did not fit to one but in this case we could add some more elements from other students projects. Do not forget to add points that indicate the location of the screws. The screws are there to attach the board to the bed of the machine. In placing the elements, the distance between them should be twice the diameter of milling end plus 10mm. In our case, it was 22mm.

![]({{site.baseurl}}/parts_distributed.png)
*arrangement of elements*

--> Fabrication included 3 files:

engraving (names, numbers of elements) / points

profiling (outcut) / boarders of our elements

pocketing (incut) / pockets

![]({{site.baseurl}}/board_yellow.png)
*preparing file RhinoCam*

![]({{site.baseurl}}/milling_gif.gif)
*RhinoCam CNC milling settings*

Do not forget to change the view in Rhino for perspective to make sure that your gonna cut in the board not somewhere on the air.

![]({{site.baseurl}}/pocketing.png)
*3D view checking the cutting file*

--> milling

The prepared file open in a program dedicated to the machine. We used KinetiC to control the machine. First stem we put the board on the bed of the machine to engrave the holes for screws to attached the board. Before engraving set the zero coordinates of the machine. We used end milling 6mm upcut.

![]({{site.baseurl}}/cnc_machine.jpg)
*KinetiC*

![]({{site.baseurl}}/board_playwood.jpg)
*preparing to milling/ setting the machine*

![]({{site.baseurl}}/cutting_process.jpg)
*cutting process*

![]({{site.baseurl}}/cut_parts.jpg)
*fabricated parts*

Issue 1.

We has small problem with the second board. Because every board is different during the pocketing of that board milling end started to he began to pull out the board as on a photo below. We checked the file and everything was ok. We decided to change the milling end for downcut to do not waste the plywood board. It gave us better result than upcut because it pulled the material down not up.

![]({{site.baseurl}}/isse.jpg)
*issue 1*

Because of the issue 1 we had a bit of sanding afterwards.

![]({{site.baseurl}}/sanding.jpg)
*sanding*

Issue 2.

The tolerance of the inside cut was in our file was to small. We did 3mm bigger than the thickness of the board. But on the end it should be two times bigger so 3mm on the each side of the pocket. In total 6mm bigger with and high. To fix the problem we used the electronic saw and file to make the pockets bigger to be able to put the level of the shelf to the crossbar of the shelf. Thickness of our board around 16mm.

![]({{site.baseurl}}/detail.JPG)
*detail*

![]({{site.baseurl}}/bookself_assembled2.jpg)
*final shot*

## --> To download:

[files](https://gitlab.com/MDEF/barbara.drozdek/blob/master/files/week_7_cnc_files.rar)
