---
title: 15. interface and application programming
period: 09-16 May 2019
date: 2019-01-28
term: II
published: true
---

This week we (together with Gabriela Martinez Pinheiro) did an interface that is connected  to our input board. During the 10th week of Fab Academy we fabricated the hello mag board that responds on the magnetic fields. We decided to work with this board again in the week of interface and application programming. We already use very simple interface to visualize the sensor working. It was a simple slider that changes the value relative to the position of the magnet.

![]({{site.baseurl}}/15_gif2.gif)
*slider interface*


[Here](http://archive.fabacademy.org/fabacademy2016/fablabbcn2016/students/346/Week16.htm) you can find Caroline's documentation of interface from Fab Academy 2016 Barcelona - How to make almost everything: We used her documentation as a reference to build the interface for our hal sensor board.

To build the interface we used the Processing software as previously. After connecting our hello.mag.45 board to Gabi's computer we had to change the USB port number according to the computer (COM4).
For the serial communication in processing we have to use the serial class, it is always at the beginning of the code

import processing.serial.; // serial class

Serial myPort; // Create object from Serial class

In void setup():

myPort = new Serial(this, "/dev/ttyUSB0", 9600);

In void draw():

You can use this commands to read the passes value of the data from the board to Processing:

myPort.available() // If data is available

myPort.read() // reading from the serial port

 Caroline's code was based on the cube that change the colour depends of the position of the geometry. The code basically displays the geometry with various of colours and gradient of shadow. First we started play with the colours of the sphere. We found some tutorials on the [Processing website](www.processing.org). We ended on using three colours: purple, magenta and cyan. You can define the colour with three (RGB) values between 0-255.

![]({{site.baseurl}}/15_dircolour.jpg)
*RGB values of the colours in our java code*

We changed the shape on the 2D (triangle). In processing there are only two 3D shapes available: cube (box) and sphere. First we changed the geometry for sphere but because of the characteristic shape the colours did not changed. This was a reason to use 2D shape in this case. It is also possible to custom these shapes by calling “vertex()”. Tutorial you can find [here](https://processing.org/tutorials/p3d/). PShapes are the primitive simple shapes but it was enough for us to show how the sensor works.

![]({{site.baseurl}}/15_geometry.jpg)
*triangle shape code line*

Finally we were with the work that we did. This is the time to start play with it!

![]({{site.baseurl}}/15_gif.gif)
*interface final show*

## --> To download

[files](https://gitlab.com/MDEF/barbara.drozdek/blob/master/files/week_15_interface%20and%20application%20programming.rar)
