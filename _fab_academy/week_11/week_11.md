---
title: 11. output devices
period: 03-10 April 2019
date: 2019-01-28
term: II
published: true
---

In this week I worked With Gabi on the output device. We decided to pick the DC motor to check how it works for our future project as I mentioned in previous submission we want to make a pottery wheel. In this case the choice was easy because we did not want to measure the exact angle of rotation but the number of turns per second and the speed has to be quite fast. We were following the classes with Victor to get more information  about all of the motors that are available in our Lab. Link [here](http://fab.academany.org/2019/labs/barcelona/local/clubs/electroclub/outputdevices/).

![]({{site.baseurl}}/11_hello.H-bridge.44.png)
*hello H-bridge board*

In this week the same like in the week before we used files from class page we took all of the information needed to making the hello.H-bridge.44 board, the one able to control a DC motor. Some more information about bridge electronic circuit [here](https://en.wikipedia.org/wiki/H_bridge). We got the files of the traces and the outcut lines, processed it on [fabmodules](http://fabmodules.org/) opensource platform.

## -->milling

First the incut:

![]({{site.baseurl}}/11_incut_trace.png)
*incut traces*

![]({{site.baseurl}}/11_incut_seyyings.JPG)
*incut settings*

Than the outcut:

![]({{site.baseurl}}/11_outcut_trace.png)
*outcut traces*

![]({{site.baseurl}}/11_outcut_settings.png)
*outcut settings*

## -->soldering

**List of components:**

1x Attiny 44

2x capacitor 1uF (c1/c2)

1x resistor 10k (r1)

1x connector ISP 6-pin

1x 4-pin connector(j2)

1x A4953 - H-bridge motor driver

1x capacitor 10uF

1x 4-pin connector (j3)

1x regulator IC2 - 5V

On previously milled board we started to solder component from the list above. I like to start with the Attiny 44 and follow the traces but it is very personal way of doing it and I recommend to find your own technique.

![]({{site.baseurl}}/11_soldered.JPG)
*soldered H-bridge board*


As always after soldering we have to check the continuity between the components in the board. We used multimeter to do this in the mode with rotated (90 degrees) an icon of WiFi.
everything was fine so we could start programming the board.

![]({{site.baseurl}}/11_multimeter.JPG)
*checking the continuity*

## --> programming

To program the output board we used my FabISP that I has done previously during the electronic design week. We connected H-bridge board with 6 pins connector cable in the way both board's traces were matching in the right order, so VCC to VVC and GND to GND on my FabISP and on the output board. This time we used Arduino software to program the board. We picked the simple example blink. We changed the pin number following the schematic below. In our case it was a pin number 2. Into this pin we soldered the output (H-bridge motor driver).

![]({{site.baseurl}}/11_schematic.png)
*checking the continuity*

We changed in our blink file the Arduino settings like: board, processor, internal clock and the most important programmer for USBtinyISP.

![]({{site.baseurl}}/11_settings_arduino.png)
*Arduino settings*

During the programming our board is connected to the power from both sides from the computer and from the 9V battery. It is important to burn bootloader very fast to not burn our output board. So the process of loading should be very fast and after that step remember to immediately disconnect the board from the battery or computer. Thanks to Edu's help, we managed to program the board in the first try.

![]({{site.baseurl}}/11_testing.JPG)
*final circuit*


##--> To download

[files](https://gitlab.com/MDEF/barbara.drozdek/blob/master/files/week_11_output.rar)
