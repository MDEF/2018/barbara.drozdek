---
title: 04. electronics production
period: 14-20 February 2019
date: 2019-01-28
term: II
published: true
---
Task for this week is to make an in-circuit programmer by milling the PCB, soldering the components and programming it. I had only experience with soldering before this week. Sounds like big challenge :)

---
# milling a PCB board

* traces
I downloaded the hello.ISP.44.traces .png file from FabAcademy website. For cutting inner traces l used 1/64” end mill. To prepare .png file for milling process l put it on the FabModules page, selected the output file as Roland mill (.rml), PCB traces (1/64), machine: SRM-20, Origin: X0, Y0 and Z0 to 0mm, in order to keep the same origin as the locally defined in the machine, zjopg - 12, speed - 4 mm/s. Last steps are **calculate** and **save**.

![]({{site.baseurl}}/incut.png)
*incut*

* outcut
I downloaded the hello.ISP.44.outcut .png file from FabAcademy website. For cutting the frame l used 1/32” end mill. To prepare .png file for milling process l put it on the FabModules page, select the output file as Roland mill (.rml), PCB traces (1/32), machine: SRM-20, Origin: X0, Y0 and Z0 to 0mm, in order to keep the same origin as the locally defined in the machine, zjopg - 12, speed - 1 mm/s for new end mills. Last clicks **calculate** and **save**.

![]({{site.baseurl}}/outcut.png)
*outcut*

* machine
The procedure to start cutting starts with setting up the end mill, position the end mill in the 0 position through the software, press x/y and z to set the parameters as the chosen ones. Ready to **cut**  window will open through which it is possible to select the .rml file that l created in Fab Modules. Select it and click output, the process will start.

![]({{site.baseurl}}/machine.jpg)
*machine settings*

![]({{site.baseurl}}/process.jpg)
*process of milling*

![]({{site.baseurl}}/final_milling.jpg)
*final milling result*
---
how to use the milling machine and prepare the file [tutorial](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/srm20_windows.html)
---
# components collecting

  Components list:

1x AT Tiny 44 microcontroller

1x Capacitor 1uF

2x Capacitor 10 pF

2x Resistor 100 ohm

1x Resistor 499 ohm

1x Resistor 1K ohm

1x Resistor 10K

1x 6 pin header

1x USB connector

2x jumpers - 0 Ohm resistors

1x Crystal 20MHz

2x Zener Diode 3.3 V

1x USB mini cable

1x ribbon cable

1x 6 pin connectors

![]({{site.baseurl}}/components.png)
*cheat sheet where to solder components*
---
# soldering

![]({{site.baseurl}}/solder.JPG)
*soldering*

![]({{site.baseurl}}/multimeter.jpg)
*checking continuity by using a multimeter*

 * There is one mistake on the picture, I marked it. In red circle there is not correct mode of the multimeter. It has to be with the icon that looks like WiFi icon rotated by 90 degrees. You can change the mode pressing the yellow button.

![]({{site.baseurl}}/final.jpg)
*final result*
---
# programming

I started follow the [tutorial](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/fabisp.html) FabISP: Programming on the Fab Academy website. I talked with tutors and they said that is very difficult to program the board on Windows computer( I have Windows10) and they they suggested me using Linux or Mac.

1. I installed Ubuntu Software on my computer

2. I downloaded and Unziped the Firmware / error in installing

* to check that my board works, I connected it to the working FabISP. Green Light on it means that my board is soldered correctly and is getting power. On the end it did not work on my computer. So I decided to use Mac computer by repeat the process for Mac.

![]({{site.baseurl}}/programing.jpg)
*programming using the FabISP*

1. In the command prompt type:

>- cd Desktop/firmware

>- make clean    

>- make hex

>- make fuse

>- make program

* The Mac computer did not recognize my board. I checked it on Linux and on Windows and it showed up in Devide Manager. The mission completed successfully. What was the problem? I have no idea. But I breathed a sigh of relief as my computer recognized the board.

<a href="code.txt" code</a>
<a href="code.rar" code</a>

Thank you Santi!

![]({{site.baseurl}}/fabisp.png)
*my computer recognized the board :)*

The last step is to unsolder 0 Ohm jumper (SJ2).

---
After this week, I do not believe people who say that they know electronics because this thing can not be assessed or measured. Well, unless you're a genius like Guillem.
