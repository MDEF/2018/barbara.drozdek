---
title: 08. embedded programming
period: 14-20 March 2019
date: 2019-01-28
term: II
published: true
---

I programmed my flexible board to blink!

---
### --> data sheet

To be an expert in electronic to should read and understand data sheet of the component that you use. In this board project we used Microcontroller [ATtiny44A](http://academy.cba.mit.edu/classes/embedded_programming/doc8183.pdf). It was very useful when I wanted to check the pin configuration to program the board using the Arduino software. the configuration on the schematic is different than output in the code. I explain it below.

---

### --> programming an ATtiny44A

For programming my board I followed the tutorial from this website:

[http://highlowtech.org/?p=1695](http://highlowtech.org/?p=1695)

--> library installing steps:

* Open the **preferences** window in the Arduino software --> **file**.
* In the line **Additional Boards Manager URLs** paste this URL (https://raw.githubusercontent.com/damellis/attiny/ide-1.6.x-boards-manager/package_damellis_attiny_index.json)
* Click the OK button to save your changes
* Open the **boards manager** in the **Tools** -->  **Board**
* Search for “ATtiny”
* Install library

![]({{site.baseurl}}/preferences.jpg)
*preferences changing*

![]({{site.baseurl}}/library.png)
*adding library*

--> connect the board to the computer and then change the variables in your Arduino program

* **Board** > Attiny/24/44/84
* **Processor** > Atinny44
* **Port** > miniUSB
* **Clock** > external 20 20MHz
* **Programmer** > USBtinyISP
* --> **Tools** > **Burn Bootloader** to apply the changes

![]({{site.baseurl}}/toolsettings.jpg)
*tool settings*

--> programming the flexible board using FabISP

I selected the “Blink” file from the example’s of Arduino. Before sending the program to the board I changed the pin number. I checked the Arduino pinout and translated info to the ATtiny pinout by using this explanation. For my Attiny the pinout of the led is 7. After I changed the pinMode output in the Arduino program I uploaded it to the board.

![]({{site.baseurl}}/serveimage.jpg)
*pinout*

![]({{site.baseurl}}/pin_blink.png)
*pin number in the program*

It was not working at the beginning. I could not upload the program because I forgot to unsolder one of the 0 Ohm jumper. This jumper causes that the same board can not be programmed again because there is on it "teaching" program. I unsoldered jumper and programmed the board again. It works! :)

![]({{site.baseurl}}/blinking.jpg)
*my board blinks :)*

---

### --> To download:

[files](https://gitlab.com/MDEF/barbara.drozdek/blob/master/files/week_8_emboded_files.rar)
