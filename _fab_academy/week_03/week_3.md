---
title: 03. computer-controlled cutting
period: 07-13 February 2019
date: 2019-01-28
term: II
published: true
---

## --> laser cutting

**press-fit** bike chain

I decided to design a chain like the bike chain using two different thicknesses of acrylic board 3mm and 5mm. I used parametric software to have a possibilities to easy manipulate the shape of the curve and it's size.

![]({{site.baseurl}}/code.png)
*design script*

![]({{site.baseurl}}/chain.gif)
*parameters*

To export curves from the grasshopper file to Rhino I did the same as l did with my italic shelf. I selected the last component in my code that represents curves in the algorithm. Using the option bake I saved it in one chosen layer in Rhinoceros.
First test the settings of the laser by making the simples shape that you can cut before your file. You have to pick the settings dedicated to the type of the material that you use and the thickness of the material.

***cutting settings:***

material: plastic - acrylic casted

thickness: 3mm

power: 80

speed: 0.50

Hz: 1000

-- for cutting the 5mm thickness acrylic joints I increased the power to 100

***engraving settings:***

material: plastic - acrylic casted

thickness: 3mm

power: 75

speed: 0.90

Hz: 20 000


![]({{site.baseurl}}/test_cut.jpg)
*laser test cutting*

**Tip!!!**
Take off the protecting plastic layer from your acrylic board. It changes a lot in cutting settings.

![]({{site.baseurl}}/cutting.jpg)

Because of the thickness of the laser I was looking for the best the optimal size of the shape of the chain and the size of the hole in it. This pieces I cut from the 3mm acrylic board, searching the optimal size of the joint that I could cut from the 5mm acrylic board and It will fit was the most difficult part in this process.

![]({{site.baseurl}}/3sizes.png)
*desiging different options/ checking the diameter of the 5mm thickness joint to fit in the hole on the shape of the chain that has 3mm diameter and thickness*

![]({{site.baseurl}}/checking.jpg)

![]({{site.baseurl}}/asemling.jpg)
*checking different options/fitting*

![]({{site.baseurl}}/checking1.jpg)
*final model*

![]({{site.baseurl}}/checking2.jpg)
*final model*

I would not say the experiment was successful in 100 percent. But from the tests that I made, I realized that at the 3mm diameter hole in the shape of the chain the joint with a diameter of 3.2mm fits the most. :)

---

## --> vinyl cutting

**stickers** girls

When I lived in Israel I used to do in a night with my fiends to the places where we could express ourselves in more creative way and sometimes not legal. Street art in Tel Aviv is very popular among design students. The stickers that I have done is based on one of my drawing on one of the streets of Tel Aviv.

![]({{site.baseurl}}/wall.jpg)
*drawing on a wall - Tel Aviv 2017*

To transform the image of the drawing for vector files (this kind of file I needed to prepare for vinyl cutter) I used Adobe Illustrator.

![]({{site.baseurl}}/photoshop.png)
*Adobe Illustrator - place*

To translate the image for vectors I used the option **image trace** make and after that expand. Next I **ungrouped** the object and deleted the background. All off the curves I joined in one curve using **pathfinder** function.

![]({{site.baseurl}}/image_trace.png)
*image trace transformation in Adobe Illustration*

The exported file in .dxf format I opened in the program dedicated to the vinyl cutter Silhouette cameo. I opened the .dxf file, I put the measurements of the vinyl piece l was going to use. I resize my drawing to fit two smaller stickers into the vinyl piece. I pressed start to start cutting.

![]({{site.baseurl}}/vinyl_cutter.png)
*cutting process*

![]({{site.baseurl}}/image_trace.png)
*result*

The next stem after cutting is to take out the pieces of vinyl that are no needed. I used tweeze because my image is very detailed. Therefore, to stick the sticker on the target object, use a masking tape to avoid losing any of the elements.

![]({{site.baseurl}}/tweezer.jpg)
*removing not needed parts*

![]({{site.baseurl}}/revers.jpg)
*result*

![]({{site.baseurl}}/masking_tape.jpg)
*transfer using masking tape*

![]({{site.baseurl}}/result.jpg)
*final result*

### --> To download:

[files](https://gitlab.com/MDEF/barbara.drozdek/blob/master/files/week_3_laser_files.rar)

---

**silk screen printing** Ryota's bday gift

![]({{site.baseurl}}/sillk_screen.jpg)
*silk screen printing*

I was also a co-creator of the birthday present for our friend from the group. The entire process is documented on the [Fífa Jónsdóttir](https://mdef.gitlab.io/fifa.jonsdottir/fabacademy/week-3/) website.
