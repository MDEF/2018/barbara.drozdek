---
title: 13. networking and communicating
period: 24-30 April 2019
date: 2019-01-28
term: II
published: true
---

This week submission I did with Gabriela again. We decided to make a circuit between two Arduino. We decided to use a push button and the LED light based on two Arduinos a slave and the master make a communication flow. We were more interested on how it works from the code side than the hardware, so that why we a picked very simple parts. In this project, we used two Arduino Unos, one to act as the "master," and one to act as the "slave".


This week we were searching different examples of communicating and networking exercise. We found a lot of references :

[led to led communication](https://www.instructables.com/id/Arduino-Communication/)

[PC to Arduino message](https://www.youtube.com/watch?v=km9XDErG2PY)

[potentiometer to led](https://www.instructables.com/id/Communicating-With-Two-Arduinos/)

[two leds](https://www.instructables.com/id/Communication-Between-Two-Arduinos-I2C/)


**List of component that you need:**

2x Arduinos

2x 220 Ohm resistors

1x LED

1x Push-button

2x Breadboards

![]({{site.baseurl}}/13_circuit.JPG)
*circuit*

##--> Master board

First, we will hook up the master Arduino to send a signal when the button is pressed. It will then send a 0 or 1 to the slave, indicating whether to turn the LED on or off.

**Master board components:**

1x 220 Ohm resisitor

1x LED

**Pines used:**

pin 9 - Output to turn on the LED

Ground - Connect to the ground rail on the breadboard

VCC (5V) - Connect to the power rail on the breadboard

**Master arduino code**

You can find on the bottom of the website.

##--> Slave board

The slave Arduino receives the signal from the master Arduino. The signal should be either a 0 or a 1. If it is a 1, it turns its LED on, and if it is a 0, it sends the signal to turn the LED off.

**Master board components**

1x push-button switch

1x 200 Ohm resistor


**Pins used:**

pin 2 - Digital read from the button switch.

Ground

VCC (5V)

**Slave arduino code**

You can find on the bottom of the website.


[![]({{site.baseurl}}/images/13_video.png)](https://www.youtube.com/watch?v=LM8FZ3Fabpk)
*final check*

## --> To download

[files](https://gitlab.com/MDEF/barbara.drozdek/blob/master/files/week_17_woldcard.rar)
