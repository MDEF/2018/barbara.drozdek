---
title: 09. molding and casting
period: 21-27 March 2019
date: 2019-01-28
term: II
published: true
---

This week we design a minimalistic model of a necklace in a shape of cuboid. The idea is to caste it using  pine resin and all of wasted electronic components from the Fab Lab. It is a bit like mosquito preserved in amber. It is a speculative form of storing information but also our attitude towards electronic devices storing information but also highlights our attitude towards electronic devices.

---

## --> modelling

We design the model using Rhinoceros software. During the process of designing the model, we took into consideration the size of the end mill and the size of our wax block. Very important thing is to remember that  both the model and the form as well should be about **3%** larger on the sides from which it will leave the mold - negative and the model - mold. Size of the end mill in our case was 3.175mm. We did the holes bigger to make sure that the endmill during the milling will consider it during the milling.

![]({{site.baseurl}}/9_tech_draw.png)
*2D drawing with dimensions*

![]({{site.baseurl}}/9_rhino_model.png)
*3D model Rhino*

---

## --> milling the wax block

*  First we melted and casted recycled wax in plywood form. We did it on the cooker in a pot that is dedicated for wax. The form we got from our classmate. It is bigger than our model all the sides are connected wit masking tape. So after the wax will be solid again inside it is easy to take it out.

![]({{site.baseurl}}/9_casting_wax.JPG)
*preparing the wax*

![]({{site.baseurl}}/9_melted-wax.jpg)
*melted recycled wax*

![]({{site.baseurl}}/9_wax_form.JPG)
*cooling wax in the polywood form*

* To make the wax block flat, we used big CNC milling machine. We created a rectangle in  Rhino bigger than dimensions of our wax block (73x170mm) to make sure that the machine will cut it. Using RhinoCAM we prepared file as for pocketing during the [week_07](https://mdef.gitlab.io/barbara.drozdek/fab_academy/week_7/). The size of end mill in our case was 10mm. Before milling we placed the wax block on a machine bed using screws and plywood boards to make it stable. Important thing is to set up the z zero coordinate of the machine in the middle of top face of your block, and xy coordinate a bit outside of the block.

![]({{site.baseurl}}/9_placening.jpg)
*the block attached to the machine`s bed*

![]({{site.baseurl}}/9_flatten_block.jpg)
*milling the wax block with 10mm endmill*

* Milling the mold in the wax block

We prepared the files in the opensource program: [Fabmodules.org](http://fabmodules.org/).

-- first step: rough cut (endmill - 1/8 flat)

- input .stl format (mesh)
- output .rlm roland milling
- process rough cut 1/8
- settings on the pictures below

![]({{site.baseurl}}/9_rough_mill.jpg)
*rough milling*

![]({{site.baseurl}}/9_result_rmill.jpg)
*result of the rough milling*

-- second step: finishing cut (endmill - 1/8 round)

- input .stl format (mesh)
- output .rlm roland milling
- process finish cut 1/8
- we used the same settings like in a rough cut process

![]({{site.baseurl}}/9_finishingmil.jpg)
*finishing milling*

The .rlm files we opened on the roland machine software starting with rough cut. We attached the our already flattened wax block on the machine bed and put the flat 1/8 end mill in the machine's culet. Next step is to set up the origin position first xy then z. In our model it was the left down corner of the wax block.

![]({{site.baseurl}}/rough_settings.gif)
*settings to prepare the file in Fabmodules program*

![]({{site.baseurl}}/9_machine_settings.jpg)
*roland software*

We ran the generated files first "in the air" to check the position of the endmill and the wax block that we positioned on a milling bed. To do this we changes the z origin position on higher than the high of our block. Everything was fine, after that we change again the z position on the high of our wax block and run the rough cut file. Went the program finished we clean the bed from extra milled wax and changed the endmill on that one dedicated to finishing cut (1/8 round). The wax block position stay the same. We have to repeat the same steps to set up the origin with new endmill. We checked the .rml finishing file by runing it "in the air" when everything is fine fun the file in the proper z position of the origin.

After all milling processes we had to make the wholes deeper using the dremel and the drilling bit of the diameter 3mm.

![]({{site.baseurl}}/9_hole_making.jpg)
*making the holes deeper*

---

## --> molding the silicon

We decide cast pine resin. It was the starting point for searching the type of the material that we can use to cast the mold. After talks with our classmate and reading the information on the packages of the silicones available in our FabLab we decided to use this [product](https://www.smooth-on.com/products/mold-max-15t/).


![]({{site.baseurl}}/9_silicon.jpg)
*silicon products*

We prepared two part silicon but first we messured the volume of our mold by putting the water in the  wax form to see more or less how much silicon we have to use to fill our form. The water from wax form we put into the plastic cup and weighed it.

![]({{site.baseurl}}/9_mold_volume.jpg)
*mold volume*

![]({{site.baseurl}}/9_cup_volume.jpg)
*mold weight*

proportion:

110g - Part A (big plastic jar)

11g - Part B (small glass bottle), 10% of the volume of the part A

In this proportion we were mixing it 3 minute to the stage that you feel that it will start to thicken. Before mixing the silicon we have to clean the wax form and protect it using the oil based spray. It reduce the possibility that the silicon will stick into the wax form.
When we have done it we could start pour the silicon. It is important that the distance between the form and the cap is quite big so the silicon stream is very thin. It reduces probable air bubbles in our mold.

![]({{site.baseurl}}/9_silicon_pouring.jpg)
*silicon casting*

Put the form with the casted silicon into the vacuum for around 10 minutes.

![]({{site.baseurl}}/9_vacume.jpg)
*vacuum*

In our case we had to leave the silicon on 24 hours. This type of silicon needs exactly this time to change physical state from liquid to solid.

![]({{site.baseurl}}/9_form_mold.jpg)
*the wax form and silicon mold*

---

## --> casting pine resin

To cast the natural pine resin we had to heat it up to around 100 degrees Celsius. We did it on the regular cooker in the pot. We were not able to cast the cast pure pine resin because after is to fragile when it again changes the physical state. So we used the proportion of 20% of beeswax and 80% of pine resin to make it more "flexible" less fragile.

![]({{site.baseurl}}/9_rasin_wax.jpg)
*resin and wax*

As an extra element inside the neckless we put an electronic component that we found on the floor in our Fab Academy room. We did it to highlight how we waste materials in our processes during that course, without taking into account how much energy and resources were devoted to it and how harmful to the environment is their production. In our project it is like a diamond a small amount information "frozen" in natural materials for next generations as an evidence.

![]({{site.baseurl}}/9_component.jpg)
*electronic component*

Every time when we casted the composite we had to wait around 30 minutes to take the pieces out of the silicon mold. Because we were not able to cast with the silicone the hole of the neckless. We did a mistake and we did not take the air from the hole after we poured the silicon inside the wax block. On the end of the casting process we have to make a hole in the final model. We did it using the hot metal wire with the same diameter 3mm. 

![]({{site.baseurl}}/9_model_dry.jpg)
*casted composite pine resin and beeswax*

The final casted product was dedicated as a bday gift for our classmate Jess! :)
![]({{site.baseurl}}/9_finalt.jpg)
*final shot*
---

## -->to download

[files](https://gitlab.com/MDEF/barbara.drozdek/blob/master/files/week_9_casting_and_molding.rar)
.
