---
title: 08. Living with ideas
period: 19-25 November 2018
date: 2018-11-25 12:00:00
term: 1
published: true
---

### Speculate, experience, learn
----

**When the technology is not ready, can we use our body to improve/ involve our object/idea? How can we link technology, reality and speculation to push our ideas forward.**


![]({{site.baseurl}}/things_on_the_table.jpg)
*artefacts*

By using the artefact from the possible present time we can start to speculate about it.  A speculation and materialization some aspects of an object is a method that helps us make our experience to. We are not only trying to design an object ( the object that does not have sens in our present world) but also speculate about the world for it which does not exist. Let's try to form the way of understanding an object in the world where we live and where we will live. It is not about what would it be like but it's more what is it like in the future or somewhere in a parallel universe. even using different techniques for to project the future, or future concept, to this time you should remember it always does not work in the way that you imagine even if you have a various different scenario for the future. The only thing is sure we can observe it and learn from the 1st person perspective and researcher perspective.


*"... a list we created a few years ago called A/B, a sort of manifesto. In it, we juxtaposed design as it is usually understood with the kind of design we found ourselves doing. B was not intended to replace A but to simply add another dimension, something to compare it to and facilitate discussion. Ideally, C, D, E, and many others would follow."*

~Speculative everything design, fiction and social dreaming Anthony Dunne & Fiona Raby


**Ex1** Material speculation

In this exercise, I picked on an object the flower pot made out of clay, not glazed (absorbs water). It has the shape of a truncated cone and natural material colour gradient. I was trying to creating a speculative artefact to materialising an abstract idea than live with it and from a first-person perspective experience it. I created a new object by modifying the previously selected one.  First I made a hole in the bottom of the pot and dragged a hempen over it. At one end of the string is poured on the second loop. In this way, I artificially extended the distance/interaction between the user and the object.  Next step exploration by asking. What if the time is physical and it is related to how an object moves? We are not able to measure the time but we can control it by rotating an object forward/backwards or just stop it.


![]({{site.baseurl}}/artificial_object.jpg)
*the physical time*


This prototype helps me to consider different situation/contexts where my object could exist and make it more concrete. We are not able to imagine or predict all of the probable scenarios of using the object. By living with it I realised that behaves and reacts differently when I am sitting, walking or standing.  I involved my body to create some kind of needed action to make my object move. I was constantly moving to keep the object moving also tried to attach the object to different parts of my body to make it more invisible to stop thinking about it like we do not think about passing time. By engaging my body I started to observe and analyze my body movement language play with the object and have fun. I realised that we do not need to put so much afford to create the situation, every smallest action triggers reaction it depends on how we design it by learning from each success and failure from the experience collected from living with our object. I  think I understand what the meaning of my actions is. I was trying to specify if the object is not only an object but maybe it is a tool or wearable. By using a hemp rope and extending the artificial object it changed my point of view of perceiving it. It made my relations/ interactions with this object changed, something appeared that made it easier for me to feel physical contact with it and how we interacting with each other.


**Ex.2** Be a wearer
![]({{site.baseurl}}/chromo_foto.jpg)
*ChromaKey app*


A fabric that acted like a computer screen and can change dynamically. How would we wear it? By using the ChromaKey app I was trying to find the border between digital and real by observing from 1st person perspective:  How do I behave with it? How do I play with it? I started thinking about different textures and tactile experience. How much can I play with the properties of physical fabric? I started to involve myself in interaction with the fabric in a different way via a screen on the device. I was trying to mimic face situation which was possible because of different background and visualization. I as a user decided to start to play with it and see what will happen. The real-life experience was a goal.


**Ex.3** Magic machine

![]({{site.baseurl}}/card-gif.gif)
*human desire*

What does it do?  It is pumping the air ( driven by your fingers movement) into your blood and it is keeping the constant movement transport the air to the cells in your body. Synthetic external heard.

![]({{site.baseurl}}/magic_machine.jpg)
*name: cold hand*


Reflection: Your outside activity is linked and effects into your inside processes. What is the movement?  The complexity of our body and all of the processes which take place in it but we do not think about that in everyday life. Can we translate and reuse the energy that we waste by changing the temperature of our body or just from a single activity like walking? How we can connect outside environment with inside environment. Can inside processes effect on our lives outside of a body and changing it for better. Can we affect them from the outside?/ Everything is moving around us matter is moving / Are we aware of what is happening in our body? It is not only the muscle movement. / prosthesis replacement / How much are we going to involve in our body to keep our species alive? Will technology help us?


Magic: you are still alive thanks to the synthetic external heard

---


**How we can change the way society perceives new/biomaterials? How to implement biofabrication in our spaces?  How to link bio labs with the society?**


From the 1st person perspective experience as a user and researcher the activity of eating with limited access to tools. The use of organic materials is allowed. With this experiment, I wanted to specify what kind of object we really need in our living spaces. Witch one we can remove or replace and witch one of them we are not able to replace.  


[![]({{site.baseurl}}/video_screenshot.png)](https://youtu.be/OkNgv70Lkhs)


Firstable it was very hard to split the user and researcher position in this experiment. The simple activity took me more time as usual. During this, I realised that the eating itself is not difficult more difficult ware activities before and after it like cleaning and preparing. That's why during the experiment I started to push myself more to extremes:  Ok, I could reduce the tools that I need. What's next? What more can I reduce? What if I can reduce the amount of water that I use only to drinking? I started to think about how can we use our biowaste which already contains the water. What if I use my biowaste to clean after eating? How can we process it? It was undeniably a more tactical experience than a taste one.  In the same time, it was very hard to control the amount of each ingredient of my breakfast. After watching the video, I noticed that I have some kind of activity rhythm one after another I wasn't aware of it.  I like to have an orderly arrangement of objects. But also the way of cultural thinking that I have to clean after it is strongly associated with how I was brought up at home.


I want to use these strategies in the future to experiment and iterate with my master project with existing limitations of the real world. I want to engage myself to make research from 1st person perspective test different solutions and scenarios to prototype a technology that doesn't exist.
