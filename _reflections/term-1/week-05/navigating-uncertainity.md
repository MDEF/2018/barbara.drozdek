---
title: 05. Navigating the Uncertainity
period: 29 October - 4 November 2018
date: 2018-11-04 12:00:00
term: 1
published: true
---

# Matter matters

**Getting rid of hierarchy and borders in the way we perceive forms as objects from which the world around us is built, but also excluding our sense, thinking or habits affect the understanding and feeling of matter.**


![]({{site.baseurl}}/plastigromerate.jpg)
*This and all subsequent images: plastiglomerate samples/ready-mades collected by geologist Patricia Corcoran and sculptor Kelly Jazvac at Kamilo Beach, Hawai’i, 2012.Photos: Jeff Elstone.*

If the matter from which our world is built is amorphous, what determines how we perceive it? Matter can be seen as movement and constant change. According to Jane Bennet American philosopher the, author of Vibrant Matter,

 ***“This is because to live, humans need to interpret the world reductively as a series of fixed objects. a need reflected in the rhetorical role assigned to the word material.”***

Both the adjective and the noun link the same unshakable, hard as rock reality. By changing the scale, carrying out the process of transforming matter through the work of human hands, often provided with a tool or using new technologies, we are constantly trying to translate our thoughts, imagining form into a raw material matter.

The Greek word **metis** meant a quality that combined wisdom and cunning. This term, understood by researchers primarily as "practical intelligence" or "ingenuity", is a form of knowledge that is difficult to acquire otherwise than through participation and which does not really give itself to generalization. It has a non-linguistic character, based on experience and practice, which forces us to confront next, similar to each other, but not identified situations requiring quick adaptation. In ancient Greek, the word mētis meant not only a specific type of knowledge but also wise advice thanks to this knowledge.

**“Plastiglomerate”** this term means most specifically  “an indurated, multi-composite material made hard by agglutination of rock and melted plastic. It is a new form of a geological artefact produced in cooperation human with nature. Plastiglomerate is an indicator already existed proof of human impact on the ecology of the Earth. This material is a reminder, an indicator of the slow violence of massive pollution. It brings together geological time and current consumerism in the form of physical matter.

By tracking the lineages of the specific word can be called a genealogy to the extent that the concept is located in its changing constitutive setting. This entails not just documenting its changing meaning etymology but the social basis of its changing meaning. Comparing those two graphs we can notice that we see the problem but we have not noticed yet the consequence.

![]({{site.baseurl}}/plastic.png)
*The graph is showing how the phrase “plastic” has occurred in a corpus of books between 1908-2008.*

![]({{site.baseurl}}/composit.png)
*The graph is showing how the phrase “plastiglomerate” has occurred in a corpus of books between 1908-2008.*

In the time of the domesticated world of Anthropocene. Where our life is shaped by cultural/ political/social changes. We can believe in the utopia scenario about our future or be humble towards nature.

***"Nature runs on sunlight.***

***Nature uses only the energy it needs.***

***Nature fits form to function.***

***Nature recycles everything.*** /I am not sure about this.

***Nature rewards cooperation.***

***Nature banks on diversity.***

***Nature demands local expertise.***

***Nature curbs excesses from within.***

***Nature taps the power of limits.""***

How to make code/ nature/ culture integrated? Why are we scared of biomaterials if we are like they are parts of nature? Are we as a human animal still part of nature?

 ---
 Reading/watching list:

Vibrant matter: a political ecology of things - Jane Bennett

Radical Matter: Rethinking Materials for a Sustainable Future - Kate Franklin, Caroline Till

Cradle to Cradle: Remaking the Way We Make Things - Michael Braungart, William McDonough

The Hidden Life of Trees: What They Feel, How They Communicate -  Peter Wohlleben

Biomimicry: Innovation Inspired by Nature - Janine M. Benyus

[Plastiglomerate](https://www.e-flux.com/journal/78/82878/plastiglomerate/) - Kirsty Robertson

[Of Blocks and Knots](https://www.architectural-review.com/rethink/of-blocks-and-knots-architecture-as-weaving/8653693.article?search=https%3a%2f%2fwww.architectural-review.com%2fsearcharticles%3fqsearch%3d1%26keywords%3dOf+Blocks+and+Knots%3a+Architecture+as+weaving) - Tim Ingold

[Chasing Ice](https://www.dailymotion.com/video/x4zovl2) - movie

[Extreme Ice Survey](http://extremeicesurvey.org/about-eis/) - project

[The Toaster Project](https://www.ted.com/talks/thomas_thwaites_how_i_built_a_toaster_from_scratch?language=en) - [Thomas Thwaites](http://www.thomasthwaites.com/the-toaster-project/)
