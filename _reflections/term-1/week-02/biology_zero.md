---
title: 02. Biology Zero
period: 8-14 October 2018
date: 2018-10-14 12:00:00
term: 1
published: true
---

## What is a role of a designer?

**In the last 20 years people in the west have undergone a huge mental transformation. From the industrial society, we have morphed into a society of knowledge and creation.  Designers create on the edges of art, technology and natural sciences by designing new recipes.**

![]({{site.baseurl}}/biology_zero.jpg)
*SCOBY packaging production process/ aquapioneers- Green FabLab/ bioreactor- MAD workshop/ lichen from the Polish forest*


When the matter leads to the form. Materials are an environment. *"We are built of the matter and we live among the matter. Our goal should not be the renunciation of matter, but the search for a form of building material other than objects. Will this form be called architecture, garden, technology - it does not matter. "* [K. Kuma, Complete Works 2012]. Material driven design is a designing process initiated in a form of the material, circular dialogue between the human and the tool, the tool and the material, the material and  the human. In this process we are trying to translate from the analogue to the digital. How can we translate the knowledge from hands to the head to create?

Making/Growing. Making and growing are two types of creation. One designed by  a man, modeled on nature, the latter designed by the nature, programmed by man.

*"What does it mean to do things. Is doing a hierarchical one assembling previously made parts into a larger whole and those wholly in even bigger whole, until everything is connected with each other? Or is it rather like a weave pattern of ever-developing threads that wrap and loop one around the other, growing constantly and never reaching the end? Doing is building or winding? "* [T.Ingold, About blocks and knots,2015].

The difference between living objects that grow and those made of elements, that have become so popular after the industrial revolution, imposed certain requirements and standards of mass production. Each part, produced in the process of mass production of objects that comes off the production line, has its specific role in the designed system. In the case of nature, every element belonging to the system has its own specific function. The difference is that nature is able to adapt to different conditions so that it functions properly and is in a continuous process of change. The difference also lies in our perception of the world of matter. If ,in such the case, doing things is combining previously designed, sketched, thought-out shapes, forms, elements and parts of the whole, which then we realize in the materials? In all forms created in the process of growth or movement, the function of a connector, hinge or glue is not a separate element, it is an implied part of the whole. Thanks to this independence and freedom, we can see the value of material amorphousness. In both cases, both forms with genetic origins in nature and origins from the designed pattern, one can talk about the division between growth and making.


Nowadays, the designer is so heavily involved in innovation of agricultural production, agri-food and agro-industrial productions and transformations as well as robot implication. Can we say that in the future every farmer will be a designer? If, through changes in the food production system, we will be able to reduce the distance between the producer and the consumer to a minimum. Speculating, in about 20 years, this system will be balanced and optimized so that it can be adapted to the space of our house by creating a circular economy on a small scale. In every house we will have a vertical crop cultivation, the main source of the bioreactor, the algae cultivated in it, will be supplemented with our diet in the daily amount of the means of contamination, an air purification system based on the logic of growth of lichens. Does this mean that in 20 years we will all be farmers 4.0 in our own homes?

The World Economic Forum published a list of skills for 2020. It includes, among others: cognitive flexibility, solving complex problems, critical thinking, managing people, creativity, emotional intelligence.  Are those skills needed to be transformed? Can design be transformative?


**hypothesis**

LICHEN WILL REDUCE POLLUTION IN THE CITIES IF WE GIVE THEM OPTIONAL CONDITION TO FUNCTION.

**methodology**

- Lichen will be domesticated.
- The city will be changed based on a lichen structure.
- We will test all of the species of lichen in different environmental conditions.
- The pattern of commuting will be studied.
- Lichen logic will be researched and studied.
- The code will be tested in the existing cities.
- The nature of lichen behavior in different natural conditions will be analyzed.
- The adaptive capacity of lichen in the city will be studied.
- The optimal condition structure for the lichen will be built in the cities.
- The pattern of lichen structure will be implemented in the city as one extra layer to filter the air.



***
  Resources:


  [https://www.scobypackaging.com/](https://www.scobypackaging.com/)

  [http://aquapioneers.io/](http://aquapioneers.io/)

  [http://www.designsummerschool.com/mad2017/](http://www.designsummerschool.com/mad2017/)

  [https://www.weforum.org](https://www.weforum.org)


{% figure caption: "*Markdown* caption" %}
![]({{site.baseurl}}/transfolab.jpeg)
{% endfigure %}
