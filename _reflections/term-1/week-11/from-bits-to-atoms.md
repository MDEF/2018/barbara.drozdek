---
title: 11. From Bits to Atoms
period: 10-16 December 2018
date: 2018-12-16 12:00:00
term: 1
published: true
---
## Parade of failures - the guide on how  do not  run a group project

1. Be always late for classes, this is the first opportunity to show your disrespect for people in the group.
2. Even if you are late, do not bother to inform your teammates about it.
3. Never be honest about what you think about the ideas of your colleagues.
4. Never say that you feel bad about the concept of the project and that you are not convinced that it makes sense.
5. I never say what you do / do you work at the moment.
6. Never start the day with a short conversation about what happened the previous day and what is the work plan for today.
7. Do not create a common folder in chmuche where you can share photos, files, videos.
8. Do not upload media files regularly so your colleagues can use them.
9. Do not try to write a few words about what you have been working on, what has happened, what to learn, after a day of work on the project website.
10. Never tell your colleagues that you have a problem and you do not know how to solve it.
11. Do not learn new skills from your group members.
12. Do not tell your group what you would like to learn while working on this project.
13. Never say that you disagree with something.
14. Do not share your knowledge with group members.
15. Do not engage in work if you see that someone in the group is not as involved in the work as you are.
16. Do not feel obliged to reply to group messages.
17. It does not document anything.
18. Do not take pictures.
19. Do not film things horizontally.
20. Do not ask for help.
21. Do not meet the deadlines.

machine project website: [DJ pom pom](<https://mdef.gitlab.io/dj-pom-pom/>)

---
## links:

[<https://bitsandatoms.gitlab.io/site/#evaluation>](<https://bitsandatoms.gitlab.io/site/#evaluation>)

[<http://fabacademy.org/2018/docs/FabAcademy-Assessment/index.html>](<http://fabacademy.org/2018/docs/FabAcademy-Assessment/index.html>)
