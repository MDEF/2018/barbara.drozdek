---
layout: page
title: Shenzhen
permalink: /research trip/
---


Our journey began almost 10,000 kilometers away from home. In a major city in Guangdong Province, China. Shenzhen forms part of the Pearl River Delta megalopolis. The city is located north of Hong Kong Special Administrative Region and holds sub-provincial administrative status. This world capital of hardware is the fastest growing city on the globe.  

[![]({{site.baseurl}}/images/research_trip/vimeo_screenshot.png)](https://vimeo.com/313128724)

video by [Fífa Jónsdóttir](https://mdef.gitlab.io/fifa.jonsdottir/)

Me together with my friends from the biology group, according to our broad understood interest in biology, thrown into deep water, we started hunting. We were looking for interactions between people, biology and technological systems. In the area of plant intelligence, fungi as remediators, biomaterial driven processes, relationship with micro-organisms, living materials. We started at Huaqiangbei the biggest electronic market on the world. It can already be seen here that the scale had a huge impact on our perception of this environment. I want to point out that not only quantitative scale but also qualitative.

![]({{site.baseurl}}/images/research_trip/market.jpg)

# Design drives business - industry creates a prosperous

***> design for beauty***

Personal hygiene in public spaces is a strongly cultural rooted issue that has a strong influence on the behavior of Chinese society. You can notice it in everyday life on city streets. Masks as an agent  in relationship to microbes and diseases. You can find two kinds of masks used for personal hygiene one that protect you from air pollution one that protect others when you are sick. It shows that Chinees society is aware of microbes in the city in the air. Does it mean that they care about environment cleanness? On the electronic market we found lots of product dedicated to the skin care. In the regular shop beauty product ware communicated as a solution not only problems with skin care.

![]({{site.baseurl}}/images/research_trip/care.jpg)

***> design for require***

 From my personal perspective the world that is assigned to the term sustainability is environmental. In harmony with nature, respecting its rights and being aware of the consequences that our actions will bring to the environment in which we live our children will live in the future. Is it gonna be the same? I can say not that it will not be. Business sustainability it was the first thought that came to one of the studio director head. There I experiences the clash of way of thinking from two as you can see different perspectives. But maybe we can change it by pointing out what is natural can be a driver for economic development as well. Green as an economic motivator. How to transform green spaces as a commodity, primarily as a recreation spaces? Highlight the difference between organic and artificial environments, products and spaces by valuing the aesthetics as a tree antenna by [Kaal Masten B.V.](https://www.nextnature.net/2009/08/antenna-tree-mast-safari/). Referring to the city how green spaces affect the health and structure of the city?

![]({{site.baseurl}}/images/research_trip/public.JPG)

***> design between craft and manufacture***

Almost 90% of work in the factories is automated. But in this world we still need people that are specialized in work and in relationship to what they do - specific and good at what they do. The 10% of the manufacture process is the part that has the potential to create a stronger relationship with the making and the outcomes. They attitude and cultural values that inherited over generations, traditional techniques that go with or against the material. Maybe by honouring the crafting processes we can focused our interaction with material world in the process of making with technology to be more dedicated. Tradition crafting methodologies  can help connect  the physical world in highly digitalized society.


---

# Design empower crafts, and crafts inspire design.

Craft: the reset exhibition that l visited as an extra activity was about the reawakening of crafts in contemporary design and society. It investigates the widening possibilities of crafts as an important creative impulse for China's future innovation, showing how crafts can inspire new ways of living and connecting with the world around us. **Within an international context where the local crafts are currently being re-defined and embraced. Craft: The Reset evaluates how creative practices are reinvigorating crafts from within.**

The exhibition was divided on 4 sections. the structure helped to understand or even discover how designers are **thinking through crafts, expressing new attitudes towards quality, detail, time, culture, skill, value and connection.**

—**Craft Revival.** How are traditional crafts inspiring designers? **How are we using traditional techniques to reach new audience, shapes our identities and preserve traditional crafts for next generation.**

—**Craft Methodologies.** Materials are an important part of craft and design. How are designers **experimenting with materials by de-constructing traditional crafts and re-interpreting them** through contemporary design.

—**Craft Innovation.** Craft are constantly being innovated. **Designers who are using new technologies to innovate crafts, shaping how we will relate to crafts in a digital future.**

— **Crafting a Better World. Can crafts help foster our balance with nature and humanity?** How designers are showing their **responsibility towards each other and the environment through new craft applications.**

![]({{site.baseurl}}/images/research_trip/craft.jpg)



# How can craftsmanship foster a symbiosis between technology and biological systems?

**Proposed assumptions are possible solutions to consider on the future.**

What if we could shift focus away from short term gains towards long term goals focused on sustainability ( environmental) and well-being?

Use embed attitudes and values from crafts in manufacturing processes. Point out the potential in relation between traditional crafts with networked technologies to scale up production in sustainable ways or redistributed it.

> People don't buy what you do; they buy why you do it. -Simon Sinek

![]({{site.baseurl}}/images/research_trip/circles.jpg)

Interesting project that reconnects people with end products and how they are made. [IOTea](https://iotea.herokuapp.com/) project by studio seeds. It is a smart agriculture is to apply the Internet of Things technology to traditional agriculture, using sensors and software to control agriculture production through mobile or computer platforms, making traditional agriculture more “smart”.

![]({{site.baseurl}}/images/research_trip/tea.JPG)
