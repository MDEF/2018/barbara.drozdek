---
title: design for the new
period: 26-17 April/May 2019
date: 2019-01-28
term: II
published: true
---
The practice of making in our everyday life. In this course, I am exploring the social practice of making the image: How do we imagine it as a practice? Studying the stuff connected with that practice tools, object, places that we need to perform this practice and the leas but not least the skill. In making the skills is the key point, different techniques, knowledge, experience. But also the tradition and cultural influence on creation.


![]({{site.baseurl}}/new_mapping.jpg)
*practice mapping*

**Social practice connected with making:**

* sharing knowledge

* designing

* exploring

* learning

* exchanging knowledge

* co-creation

* spending time with others

* collaborating

*  do it yourself

* participatory design

![]({{site.baseurl}}/new_now.jpg)
*current practice - making craftsman*

![]({{site.baseurl}}/new_future.jpg)
*future practice - digital craft*

Creation/making as a social practice. Making things has been shaping our society in the past no and how is it going to shape it in the future where technology influence on creating a new practice. The fundamental human need that we have is creation.

need (our fundamental human need) - Creation

be (being qualities) - imagination, boldness, inventiveness, curiosity

have (having things) - abilities, skills, work, techniques

do (doing actions) - invent, build, design, work, compose, interpret

interact (interacting settings) - space for expression, workshop, audiences


**Future projection:**

![]({{site.baseurl}}/new_projection.jpg)
*Intervention transition pathway*

**Why?**

I am focusing on making via disappearing practices and techniques in crafts. I think that people need to develop their skills in closer contact with the matter in physical contact and dialogue with the material. How the knowledge of making from the past will drive the way we make things in the future. How the new emerging professional profile of the craftsmen is going to look like in the future?  

**Where?**

Due to make space and maker movement in the cities and crafts/techniques regions. How we can link those people and their skills? How can we foster the rural area where are the roots of craft that on the beginning was a cottage industry and had an image of community work. Is it possible to make rural areas attractive again for young people?

![]({{site.baseurl}}/new_interwention.jpg)
*prototype/proposition of intervention*

The problems that I am facing in my project is connected with craftsmanship and diapering professions and infrastructure to develop skills for professional makers. The solutions that I am proposing is a new choreography of making, new digital craftsmen profile, co-creation with maker people to drive a new way of making and developing new skills and techniques of making on the edge of the physical and digital world. The systems where my project impacts are the creative economy and the people with whom I work are craftspeople and maker people. The fact is that the growth of automatization. But what can be automatized and what cannot be? Digital cultural heritage in the 21st century is not defined and this is an emerging field of our knowledge society. The future is plural, we can preserve the knowledge of the craftspeople by co-creation between them and maker people that provide new technology that influences on creating a new practice.
