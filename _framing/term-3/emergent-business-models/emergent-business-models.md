---
title: emergent business models
period: 00-00 May/June 2019
date: 2019-01-28
term: II
published: true
---

This course was led by the people from [Ideas for change](https://www.ideasforchange.com/) the organization that explores open, collaborative strategies for exponential growth. During those three sessions, we were building our emergent business model for our master projects. We were introduced to the impact and levels of it. At what level do you want to impact?
There are few of them: by direct service or many direct services, another option is system change and the most challenging in my opinion change of paradigm.

What can I do to generate impact, what level of impact?

  <iframe src="https://docs.google.com/a/iaac.net/presentation/d/e/2PACX-1vQykyJq8dFj_3RrRUV8k2MF0K0zXvKfLVZVLNB3_LgG13ngGIhkc6kgmlpQK0UbQhG9X4AfBfviIr5E/embed?start=false&loop=false&delayms=3000" frameborder="0" width="640" height="480" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
*1st session*

In this exercise, the goal was to find already existing innovations from the same system but not necessary it could be from another but with a similar strategy that is relevant to our projects. What is also important how to calculate the effort and minimize (internal & external) efforts. To minimize internal efforts - focus on one specific variable in the system. To minimize external efforts - facilitate adoption: efforts for change, build on a highly-adopted solution. In my project, the innovation belongs to the systems like the creative economy, craftsmanship, maker movement, cultural heritage. I want to change or have an impact on craftsmanship profession profile, creative collaboration with the machine as a partner in making, craftsmanship and maker movement co-creation, a new way of making.

On the second session, we were introduced to the fields of the possibilities of our interventions from the element of change, changed rules or agent perspective. The Pengrowth design methodology for impact and principles of accelerated growth. It is divided into five categories:

*  Connect in network

--> social, mobile, things

* Collect inventory

--> centralized, distributed, consumers

* Empower users

--> user, user/producer, any role

* Enable partners

--> provide, co-maker, co-create

* Share knowledge  

--> proprietary, non-commercial, open

![]({{site.baseurl}}/2_activity_ebm.png)
*2nd activity*

*If you change the way you look at things, the things you look at change.*

-- MAX PLANCK

By generating the interaction you can make a change. During the last meeting, we presented the roadmap elaborated to maximize the impact of our projects, shared our references of previous innovation to take a lesson from the past and build on the top of it the future emergent business model.
