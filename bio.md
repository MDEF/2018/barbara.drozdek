---
layout: page
title: 
permalink: /bio/
---

![]({{site.baseurl}}/images/barbara_drozdek.jpg)

I am a designer who is still looking for identity. My first plan was to become an engineer, but I switched to design. I graduated from industrial design department in School of Form. I like experimenting with materials and exploring new technologies.
Am I an intervention designer?
