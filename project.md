---
layout: page
title: project
permalink: /project/
---
### **made by us**
---

*We have been always focused on the final object that the craftsman does and impressed by his skill. But in this practice, there is bigger “hidden” information. There is huge potential and precious information coded in the movement about how we make things.*

![]({{site.baseurl}}/images/master/final_reflection/madebyus.jpg)
*embodied knowledge*

What will be the future of work all about? Making things shapes our society today, all the way to the past. Due to automation, a result of industrialisation, the role of humans has changed. There is more and more disappearing professions in a time where our profession profiles are dynamically changing. Nowadays we have a tendency to overrate technical and linguistic aptitude and undervalue manual skills. Craftsmanship is the 4th world’s largest economy. It is a way of thinking and doing where humanity is in tune with nature, not working against it. It leads to a world that’s built to last. There is something we need to move toward the values and principles of craftsmanship — autonomy, responsibility and creativity — are transforming work and management as we know it. In many ways, that’s what the future of work will be all about.

**Made by us** is a new extraordinary creative generative collaboration between human and machine, that arises from the interaction of traditional technologies and embodied knowledge of the craft people with new digital fabrication technology, data processing tools and the knowledge of makers. The project is focusing on human-machine reconfiguration in the process of making. How can we preserve the embodied knowledge of the craftsmen while using machines? By adding, subtracting or transforming material (also data as a raw material), or by combining three types of process, we could create the new in interdisciplinary co-creation between craft people, machines and makers. It is a preservation and creation of the techniques, towards a new craft that were not possible before.

## Craft processing - new digital craftsman technique

**Craft Processing** is a new craft technique, it introduces a new choreography in making based on gestures and material information. It reshapes the way of making, reveals the working components as form or performance, creates new aesthetic and experience. It is emerging by bridging the maker movement and craftsmanship in co-creation and knowledge exchange about technology and material. The process is based on a transfer of knowledge from material driven dialogue to data driven design. It is divided into three steps:

**Capturing** - capturing the information of embodied knowledge coded in the movement of the craftsman (gesture).

**Data processing** - processing the information in a way of unrolling the “string” of hidden intention that the craftsman did during the process of making.

**Fabrication** - materializing the gestures (object) of the craftsman using digital fabrication technology.

[![]({{site.baseurl}}/images/master/final_reflection/crfat_processing_v.png)](https://www.youtube.com/watch?v=Eadleex0kDs&feature=youtu.be)
*digital crfat technique video*

The object as a physical representation of re-appearance in new ways digital craftspeople. It is a new digital cultural heritage, a physicalization of the material(clay) in the shape of the emerging objects.

## Futures

The way how we study movement over generations in art, photography, craft and design or architecture shapes our vision about us in the future. In craft, movement is a gesture in an embodied knowledge of the craftsman. Technological advances have transformed the character of making. Some make a feature of miniaturisation; others reveal the working components as form or performance. Craft has a huge potential to bring human hands to makerspaces. It can also provide an increase in the diversity of techniques, high awareness of material intelligence and sustainable material flows, thus open up all new area of activities in makerspaces.

## Article

<script async class="speakerdeck-embed" data-id="da8a5be3663740558f9895634913e4a9" data-ratio="1.41436464088398" src="//speakerdeck.com/assets/embed.js"></script>

## Final reflection

How making things shape our society today and all the way to the past? The process of learning new skills enables people to develop themselves and become good citizens. Craftsmanship is a way of thinking and doing where humanity is in tune with nature, not working against it. Could everyone become a good craftsman?

Many people think that craft is a matter of executing a preconceived form or idea, something that already exists in the mind or on paper. Yet making is also an active way of thinking, something which can be carried out with no particular goal in mind. In fact, this is
a situation where innovation is very likely to occur.

The craft abilities are innate and widely distributed if it is correctly stimulated and trained over the years, they allow craftsmen to become knowledgeable public persons. The person that knows how to translate material dialogue knowledge to our everydayness. They have not only skills about making, they know how to negotiate between autonomy and authority; how to work not against resistant forces but with them; how to complete their tasks using minimum force; how to meet people and things with sympathetic imagination; and above all they know how to play, for it is in play that we find the origin of the
dialogue the craftsman conducts with materials like.

The rich material knowledge, iterative processes, collaborative value of craft make it an ideal route to introduce the unexpected in cross-disciplinary partnerships and collaboration with makers.  It can also provide an increase in the diversity of techniques, high awareness of material intelligence and sustainable material flows, thus open up all new area of activities in makerspaces.

I see huge potential to bring human hands to makerspaces due to this I think we should stop granting the lower status of craft and create the space to share their knowledge where everyone could
experience it.

## Project video

[![]({{site.baseurl}}/images/master/final_reflection/made_by_us_v.png)](https://www.youtube.com/watch?v=JgAGABs_iFg&feature=youtu.be)

## *
The project was created in cooperation with the ceramist [Marc Vidal](https://www.marcceramica.com/)

### human-machine reconfiguration
---
[![]({{site.baseurl}}/images/master/final_termll/video_termll.png)](https://youtu.be/Eq6YDeRLPAQ)
*video*

Nowadays at the beginning of the 21st century in the information society our professions profiles are changing. The phenomenon of disappearing professions is more and more visible in our society. Because of automation and automation that has evolved from industrialization human position as a instruments of labour, to create a product has been changing. Automation has resulted in a reduction of jobs in the service sector.

Despite the fact that today's craftsmanship is the 4th on the world's largest economy, the professions associated with this sector are at the forefront of disappearing professions list. This is due, among other things, to the fact that knowledge society has turned job instruments from mines, factories, machines and tools into offices and computers. Even earlier the industrial society left shovels. The education system of craftsmen has not changed since then. A dual education system which involves downloading theoretical knowledge at a vocational school and acquiring practical knowledge in craft workshops of training masters. What if such plants cease to exist when their numbers are constantly declining.

The profession of a craftsman is based on embodied knowledge that a person acquires over time. Repetition of performing the same activities, movements, habits over time, transforms from online knowledge into offline knowledge, and we can also present the priest as embodied knowledge, memory of muscles or manual / hidden skill.

### How can we preserve the embodied knowledge of the craftsmen?

on the specific craftsmanship I choose, pottery is my subject of research as a part of the knowledge society is transmitting the knowledge from material driven to data driven design. From physical to digital. From bits to atoms (and maybe back to bits). The dialogue that is between the potter and the clay. To narrow down my project I picked the special technique that is throwing on a pottery wheel.

![]({{site.baseurl}}/images/master/final_termll/history-website.jpg)
*history of the society through an object*

This is not a random choice. Examining the anthologies of the object, this technique is one of the oldest in the world and the object itself was the first mechanized object in the production of ceramics. This object has clearly undergone transformations from the beginning of agriculture to the present day. Changes did not occur only in time, but also cultural factors (adaptation) caused by the migration of technology determined the shaping of the technique.

Clay is the one of the most distributed material in the world. We can generally divide it into four quality groups. Depending on the location, the clay properties change as well. In this case, we can not talk about the adaptation of the material to the creation, but the reverse.

![]({{site.baseurl}}/images/master/final_termll/gestures.jpg)
*throwing clay on a pottery wheel | distribution of clay on the world*

### Creating a new pattern of information between human and machine will change the process of production.

This new relation between human and machine in making where both the machine and the human as well are partners in creation. In generative creative collaboration in making is a new way of producing things. It will create new aesthetics and value of things. The machine and the human has the same impact in creation. The object is bias on the input from the craftsmen throwing the pot on a pottery wheel where the gestures are a signature. It will not be made in China neither hand made. The AI material platform will help makers create compelling design of the craftsmen and machine, that are unable to be achieved any other way using the local clay to create an emerging object.

![]({{site.baseurl}}/images/master/final_termll/leap_hands.jpg)
*physical/digital interaction of the gestures*

---
### crafts - gestures coded in men's work with material
---
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTzVfBTjtUbOJ_sVzpSLF-d74OP3WoOI1I0bkmmsQhAeM6FWrhd0Fl7euGZ5Z40u5-qmp0Onw9P_9-L/embed?start=false&loop=false&delayms=3000" frameborder="0" width="640" height="480" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
*proposal presentation*
